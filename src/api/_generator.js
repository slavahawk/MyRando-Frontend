export default ($axios) => ({
  async postGeneratorNumber({ count = 1, from = 0, to = 50 }) {
    try {
      const { data } = await $axios.post("generator/number", {
        from,
        to,
        count,
      });
      return data;
    } catch (e) {
      return e;
    }
  },
  async postGeneratorVk({
    link = "",
    option = "comment",
    extra_option_list = [],
  }) {
    try {
      const response = await $axios.post("generator/vk", {
        link,
        option,
        extra_option_list,
      });

      if (response.data) return response.data;
      return response.response.data;
    } catch (e) {
      return e;
    }
  },
  async postGeneratorIg({
    link = "",
    option = "comment",
    extra_option_list = [],
  }) {
    try {
      const response = await $axios.post("generator/ig", {
        link,
        option,
        extra_option_list,
      });

      if (response.data) return response.data;
      return response.response.data;
    } catch (e) {
      return e;
    }
  },
});
