import * as moment from "moment";
import "moment/locale/ru";
moment.locale("ru");
export const Moment = (app) => {
  app.config.globalProperties.$moment = moment;
};
