import axios from "axios";
export const $axios = axios.create();
// import { api } from "@/api";
// import Toast from "@/plugins/toast";
// const store = require("./../store");
$axios.defaults.baseURL = process.env.VUE_APP_BASE_URL_API;
const token = localStorage.getItem("token");
if (token) $axios.defaults.headers.common["Authorization"] = "Bearer " + token;

// // Add a request interceptor
// $axios.interceptors.request.use(
//   function (config) {
//     console.log('r', config);
//     return config;
//   },
//   function (error) {
//     console.log('e', error);
//     return Promise.reject(error);
//   }
// );

// Add a response interceptor
$axios.interceptors.response.use(
  function (response) {
    // console.log(response);
    // if (response.data.access_token) {
    //   $axios.defaults.headers.common[
    //     "Authorization"
    //   ] = `Bearer ${response.data.access_token}`;
    // }
    return response;
  },
  async function (error) {
    const res = error.response;
    if (res) {
      // Toast.ToastForAxios(error.response);

      if (res.status === 401 && res.data.message === "VK auth failure") {
        // console.log("1");
      } else if (
        res.status === 401 &&
        res.data.message === "You are not allowed to access this page"
      ) {
        // console.log("2");
      } else {
        // const data = await api.auth.refreshToken();
        // console.log(data);
        localStorage.removeItem("token");
      }
    }
    return error;
  }
);

export default { $axios };
