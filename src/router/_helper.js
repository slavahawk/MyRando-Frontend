import store from "@/store";

// Редирект на страницу home, если ты не авторизован или не супер админ
export function redirectToHomeByDontAdmin(to, from, next) {
  const user = store.state.user.user,
    auth = store.state.auth.auth;
  if (auth && user.is_superadmin) next();
  else return next({ name: "Home" });
}
