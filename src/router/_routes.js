import { redirectToHomeByDontAdmin } from "@/router/_helper";
import { api } from "@/api";
import store from "@/store";
import Toast from "@/plugins/toast";

export const routes = [
  {
    path: "/",
    name: "Home",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Home.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/number",
    name: "Number",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Number.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/ask",
    name: "Ask",
    component: () => import(/* webpackChunkName: "about" */ "../views/Ask.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/password",
    name: "Password",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Password.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/question",
    name: "Question",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Question.vue"),
    meta: { layout: "DefaultLayout" },
    beforeEnter: async (to, from, next) => {
      await Toast.ToastForAxios("Страница в разработке");
      await next({ name: from.name });
    },
  },
  {
    path: "/fact",
    name: "Fact",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Fact.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/win",
    name: "Winvk",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Winvk.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/personal_cabinet",
    name: "Personal",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Personal.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/admin",
    name: "Admin",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Admin.vue"),
    meta: { layout: "DefaultLayout" },
    beforeEnter: [redirectToHomeByDontAdmin],
  },
  {
    path: "/auth/vk",
    name: "AuthVk",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Home.vue"),
    meta: { layout: "DefaultLayout" },
    beforeEnter: async (to, from, next) => {
      await api.auth.postAuthVk(to.query.code);
      await store.dispatch("GET_USER_ME_VK");
      await next({ name: "Home" });
    },
  },
  {
    path: "/auth/insta",
    name: "AuthIg",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Home.vue"),
    meta: { layout: "DefaultLayout" },
    beforeEnter: async (to, from, next) => {
      await api.auth.postAuthIg(to.query.code);
      await store.dispatch("GET_USER_ME_IG");
      await next({ name: "Home" });
    },
  },
  {
    path: "/offer",
    name: "Offer",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Offer.vue"),
    meta: { layout: "DefaultLayout" },
  },
  {
    path: "/:pathMatch(.*)*",
    name: "404",
    component: () => import("../views/Home.vue"),
    meta: { layout: "DefaultLayout" },
    beforeEnter(to, from, next) {
      return next({ name: "Home" });
    },
  },
];
