export default {
  methods: {
    pay(confirmation_token) {
      //Инициализация виджета. Все параметры обязательные.
      const checkout = new window.YooMoneyCheckoutWidget({
        confirmation_token: confirmation_token, //Токен, который вы получили после создания платежа
        return_url: "https://yookassa.ru/", //Ссылка на страницу завершения оплаты
        error_callback: function (error) {
          console.log(error);
        },
      });

      //Отображение платежной формы в контейнере
      checkout.render("payment-form");
    },
  },
};
