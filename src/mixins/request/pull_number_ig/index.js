export default {
  data() {
    return {
      pull_number: [],
      waitResponsePullNumber: false,
    };
  },
  methods: {
    async getPullNumberIg() {
      this.waitResponsePullNumber = true;
      this.pull_number = await this.$api.number_ig.getPullNumberIg();
      this.waitResponsePullNumber = false;
    },
    async deletePullNumberIg(id) {
      await this.$api.number_vk.deletePullNumberIdIG(id);
      await this.getPullNumber();
    },
  },
};
