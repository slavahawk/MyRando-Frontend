export default {
  data() {
    return {
      user_id: {},
      waitResponseGetUserId: false,
    };
  },
  methods: {
    async getUserId() {
      this.waitResponseGetUserId = true;
      this.user_id = this.$api.user.getUserId(id);
      this.waitResponseGetUserId = false;
    },
  },
};
