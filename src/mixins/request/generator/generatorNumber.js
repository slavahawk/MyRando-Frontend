export default {
  data() {
    return {
      generator_number: 20,
      generator_setup: {
        from: 0,
        to: 1000,
        count: 1,
      },
      waitResponseGeneratorNumber: false,
    };
  },
  methods: {
    async generatorNumber({ count, from, to }) {
      this.waitResponseGeneratorNumber = true;
      const data = await this.$api.generator.postGeneratorNumber({
        count,
        from,
        to,
      });
      data
        ? (this.generator_number = data)
        : this.$toast("Ошибка получения генерированного числа");
      this.waitResponseGeneratorNumber = false;
    },
  },
};
