module.exports = {
  // devServer: {
  //   disableHostCheck: true,
  //   port: 4000,
  //   public: "0.0.0.0:4000",
  // },
  // publicPath: "/",
  // env: {
  //
  // },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import '@/assets/scss/_mixins';
          @import '@/assets/scss/colors';
        `,
      },
    },
  },
};
